FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04
LABEL maintainer="Cecil Liu <knom1978@hotmail.com>"

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        software-properties-common \
        python3-pip \
 && apt-get install -y python python3-dev \
 && apt-get install -y build-essential \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# pip has to be installed before setuptools, setuptools has to be installed before tensorflow
RUN python3 -m pip install --no-cache-dir -U pip==18.1 \
&& python3 -m pip install --no-cache-dir -U setuptools==39.1.0
RUN pip3 install --no-cache-dir -U pillow ipython requests numpy pandas quandl scipy tensorflow-gpu==1.12 keras opencv-contrib-python opencv-python==3.2.0.8 pyinstaller

WORKDIR /root

RUN ["/bin/bash"]

