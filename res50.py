#import keras
#import numpy as np
from numpy import expand_dims
from keras.applications import resnet50
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.imagenet_utils import decode_predictions

#Load the ResNet50 model
resnet_model = resnet50.ResNet50(weights='imagenet')
filename = '/home/cat.jpg'

original = load_img(filename, target_size=(224, 224))
print('PIL image size',original.size)

numpy_image = img_to_array(original)
print('numpy array size',numpy_image.shape)

#image_batch = np.expand_dims(numpy_image, axis=0)
image_batch = expand_dims(numpy_image, axis=0)
print('image batch size', image_batch.shape)

# prepare the image for the resnet50 model
processed_image = resnet50.preprocess_input(image_batch.copy())

# get the predicted probabilities for each class
predictions = resnet_model.predict(processed_image)

# convert the probabilities to class labels
# We will get top 5 predictions which is the default

label = decode_predictions(predictions)
print(label)
