FROM cecil/dl:base_image
LABEL maintainer="Cecil Liu <knom1978@hotmail.com>"

COPY res50.py cat.jpg /home/
WORKDIR /home
CMD [ "python3", "/home/res50.py" ]
