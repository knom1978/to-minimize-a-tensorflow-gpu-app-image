The idea to minimize a tensorflow-gpu app image
===
###### tags: `tensorflow-gpu` `Pyinstaller` `Multi Stage Builds`
## Introduction
Currently containers are used in deep learning applications popularly. However, docker images with gpu support suffer large-size issue. It's a important topic to minimize the size of deep learning docker image. The below part explain how to do for the reference.

## Preparation: to build the base image with cuda support
In order to test the idea of minimizing docker image with gpu support, a py file with **keras ResNet50 model** is loaded according to the post from https://www.learnopencv.com/keras-tutorial-using-pre-trained-imagenet-models/

But before this, a base image with cuda support is necessary, and it is built from **nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04**, which is **1.23GB**. After adding related libraries, such as python, tensforflow-gpu, keras, the final image size is **2.94GB**.
```
docker build -t cecil/dl:base_image -f Dockerfile .
```

## Idea 1: to attach a python file into the base image
To simplest way to create a tensorflow-gpu application is to attach the python file into the base image directly. The detail please check idea1.Dockerfile. However, the image size is not so satisfied.
```
docker build -t cecil/dl:idea1 -f idea1.Dockerfile .
```
To run the application, you can just run as follows.
```
docker run --runtime=nvidia -it --rm cecil/dl:idea1
```

## Idea 2: to attach a compiled file into the cuda image
The second way to create a tensorflow-gpu application is slightly complicated. We use **Pyinstaller** to complile the python file and related libraries, which is inspired by https://blog.csdn.net/keineahnung2345/article/details/86305720

You can notice that **Pyinstaller** has been installed in the base image. So the remained step you can refer to idea2.Dockerfile. In the dockerfile, we use a special skill called **Multi Stage Builds**. 
```
docker build -t cecil/dl:idea2 -f idea2.Dockerfile .
```
To run the application, you can just run as follows.
```
docker run --runtime=nvidia -it --rm cecil/dl:idea2
```

## Idea 3: to attach a compiled folder into the cuda image
The idea 2 seems good, but the execution time is slightly slow. This is because the execution file unzip the dependency files to a temp folder. So we change the compile method from one file to one folder. 
```
docker build -t cecil/dl:idea3 -f idea3.Dockerfile .
```
To run the application, you can just run as follows.
```
docker run --runtime=nvidia -it --rm cecil/dl:idea3
```

## Idea 4: to mount a local compiled folder into the cuda image
Since the idea 3 gets fast but large, the next idea is to mount a local complied folder into the official cuda image. There are two steps as follows:
1. To create the compiled folder by Pyinstaller in the base image
```
docker run --runtime=nvidia -it --rm -v "$PWD":/home -w /home cecil/dl:base_image pyinstaller --onedir /home/res50.py
```
2. To mount the folder to the official cuda image
```
docker run --runtime=nvidia -it --rm -v "$PWD":/home -w /home nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04 /home/dist/res50/res50
```

## Idea 5: to use docker-compose to mount a data volume container(with a compiled folder) to the cuda image
Similar to idea 4, but create a docker image called **cecil/dl:compile** to store compile file. In other words, we separate the large application image to two: the official cuda image and compiled folder image.
If you don't like docker-compose up, you can try the below steps:
```
docker build -t cecil/dl:compile -f compile.Dockerfile .
docker run -d --name res50container cecil/dl:compile
docker run --runtime=nvidia --volumes-from res50container -it --rm -w /home nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04 /home/dist/res50/res50
```

## Comparison
**Official cuda image(nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04): 1.23GB**

**Official cuda image(nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04): 2.59GB**

**Base image: 2.94GB**

**Idea1: 2.94GB**

**Idea2: 1.95GB** => small but slow

**Idea3: 2.89GB** => fast but 

**Idea4: 1.23GB** => fast and small, but need more cares for the storage, around **1.6GB** folder 

**Idea5: 1.23GB** Official cuda image(nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04) +  **1.78GB** compile folder image

We complile the tensorflow-gpu application and mount it into a clean offical cuda image instead of just putting it into a lazy image with every needed python libraries, we save 1.6GB...still no free lunch. 

## Follow up
1. To change the style of importing python libraries: Currently the python file usually are imported the whole library. If we just import the used functions, the final image will get smaller. 
2. To use newer cuda images: cuda:9.0-cudnn7-runtime-ubuntu16.04 is 1.23GB, but cuda:9.0-cudnn7-runtime-ubuntu18.04 is smaller (1.19GB).
3. Maybe other compile tool, such as cx_Freeze, will get smaller/faster result.
4. To seperate app images to several ones, but maybe it results in other transfer efforts.
