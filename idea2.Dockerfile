FROM cecil/dl:base_image AS builder
LABEL maintainer="Cecil Liu <knom1978@hotmail.com>"
COPY res50.py cat.jpg /home/
WORKDIR /home
# Pyinstaller will generate a binary file in the dist folder,
RUN pyinstaller --onefile res50.py

FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04 AS base
# Copy the binary file from the dist folder of builer
COPY --from=builder /home/dist/res50 /home/res50
# cat.jpg is for demo
COPY cat.jpg /home/
WORKDIR /home

CMD ["/home/res50"]
