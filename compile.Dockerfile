FROM cecil/dl:base_image AS builder
LABEL maintainer="Cecil Liu <knom1978@hotmail.com>"
COPY res50.py cat.jpg /home/
WORKDIR /home
# Pyinstaller will generate a binary file in the dist folder,
RUN pyinstaller --onedir res50.py

FROM ubuntu:16.04 AS base
# Copy the binary file from the dist folder of builer
COPY --from=builder /home/dist/res50 /home/dist/res50
# cat.jpg is for demo
COPY cat.jpg /home/
WORKDIR /home
VOLUME /home
RUN chmod +x /home/dist/res50/res50
ENTRYPOINT ["/home/dist/res50/res50"]
